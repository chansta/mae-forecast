##################################################################################################
"""
name:				MC_MAD_largeK_v2.py
author:				Felix Chan
email:				fmfchan@gmail.com
date created:			2017.09.12
description:			Conduct Monte Carlo experiments on the difference in optimal weights between MSE and MAD. 
Changes:                        It allows the possibility of out sample forecasting comparison. 
"""
##################################################################################################
import numpy as np
import scipy as sp
import scipy.stats as sps
import scipy.optimize as spo
import matplotlib.pyplot as plt 
import copy as copy
import h5py as h5

def gen_cor(K):
    """
	Generate a KXK correlation matrix from a set of random vectors. 
	Input:
	    K: positive int. The size of the correlation matrix. 
	Output:
	    cor: KXK numpy array. Correlation matrix. 
    """

    A = sps.norm.rvs(size=(K,K))
    A = np.dot(A.transpose(),A)
    diagA = np.diag(np.diag(A)**-0.5)
    A = np.dot(diagA, np.dot(A, diagA))
    return A 

class MC_MAD_largeK(object): 
    """
	A class implementation of a Monte Carlo Experiment comparing the optimal weights between MSE and MAD. It generates a set of random covariates and response variable based on a given correlation matrix. 
    """

    def __init__(self, corM, incr=5, T=100, outsample=0, replication=1000, dist='norm'):  
        """
	    Inputs:
		corM: (K,K) numpy array. Correlation matrix.
		incr: Positive int. Number of models to add.  
		T: Positive int. Number of observations. 
                outsample: Float in (0,1). Proportion of T that should be reserved for out-sample forecasts. 
		replication. Positive int. Number of replications for the MC. 
                dist: choice of distributions. It can be 'norm' or 'skew-norm' 
	    Members:
		self.TK: Total number of variates (covariates + dependent). 
		self.K_all: Number of models available. 
                self.K: Number of models consider in a particlar simulation run. 
                self.T Positive int. Number of observations. 
                self.outsample: Float in (0,1). Proportion of self.T that is reserved for out of sample forecasts.
                self.T_training: Positive Int. The number of observations for training. 
                self.T_outsample: Positive Int. The number of observations for out of sample forecasts. 
                self.dist: choice of distribution. 
		self.corM: The correlation structure between the covariates and the dependent variable. 
		self.incr: Number of models to add. 
		self.replication: Number of replication.
                self.e_cov: (self.K_all, self.K_all) numpy array. Theoretical covariance matrix of forecast error based on corM. The model is assume to be $y_t = r_{0i} x_{it} _ e_{it}$. 
                self.MSE_aMSE: (self.cases, self.replication) numpy array. The MSE for all cases over all replication using estimated MSE weight.  
                self.MAD_aMAD: (self.cases, self.replication) numpy array. The MAD for all cases over all replication using estimated MAD weight. 
                self.MSE_aMAD: (self.cases, self.replication) numpy array. The MSE for all cases over all replication using estimated MAD weight.
                self.MAD_aMSE: (self.cases, self.replication) numpy array. The MAD for all cases over all replication using estimated MSE weight.
                self.MSE_aMSE_true: (self.cases, self.replication) numpy array. The MSE for all cases over all replication using True MSE weight.
                self.MAD_aMSE_true: (self.cases, self.replication) numpy array. The MAD for all cases over all replication using True MAD weight.
        """
        self.TK = corM.shape[0]
        self.K_all = self.TK-1
        self.corM = corM
        self.incr = incr
        self.T = T
        self.outsample = outsample
        self.T_outsample = int(np.floor(self.T*self.outsample))
        self.T_training = self.T-self.T_outsample
        self.replication = replication 	
        self.dist = dist
        self.cases = int(np.floor(self.K_all/self.incr))
        self.MSE_aMSE = np.zeros((self.cases, self.replication))
        self.MAD_aMAD = np.zeros((self.cases, self.replication))
        self.MSE_aMAD = np.zeros((self.cases, self.replication))
        self.MAD_aMSE = np.zeros((self.cases, self.replication))
        self.MSE_aMSE_true = np.zeros((self.cases, self.replication))
        self.MAD_aMSE_true = np.zeros((self.cases, self.replication))
        self.__gen_theoretical_ferror_cov__()            

    def __gen_data__(self): 
        """
            Generate the simulated data
        """
        if self.dist == 'norm': 
            self.X = sps.norm.rvs(size=(self.TK,self.T))
        elif self.dist == 'skew-norm':
            self.X = self.__gen_skewnorm__()
        self.A = np.linalg.cholesky(self.corM)
        self.Z = np.dot(self.A, self.X) 
        self.y = self.Z[0] 
        self.covariate = self.Z[1:self.TK]
        self.yhat_all = np.zeros(self.covariate.shape)
        for i,rho in enumerate(self.corM[0][1:self.TK]):
            self.yhat_all[i] = rho*self.covariate[i]
    
    def __gen_skewnorm__(self):
        """
            Generate skew normal variate. 
        """
        x = sps.norm.rvs(size=(2*self.TK,self.T))
        x0 = x[0:self.TK]
        x1 = abs(x[self.TK:2*self.TK])
        y = x0 + x1
        y = (y-np.power(2/np.pi,0.5))/np.power(2-2/np.pi, 0.5)
        return y

    def optimal_weight_MSE(self, cor):
        """
            Compute the theoretical optimal wegiths give self.corM. 
        """
        i2 = np.ones(self.K) 
        invC = np.linalg.inv(cor)
        A = np.dot(i2.transpose(), np.dot(invC, i2))
        return np.dot(invC, i2)/A 
    
    def __gen_theoretical_ferror_cov__(self): 
        """
            Generate the theoretical variance-covariance matrix of the forecast error based on self.corM. The forecast error is assumed to be $y_t = r_{0i} x_{it} + e_{it}$ where $r_{0i}$ is an element in the first row of self.corM. 
        """
        self.e_cov = np.zeros((self.K_all, self.K_all))
        for i in np.arange(0, self.K_all):
            for j in np.arange(i, self.K_all):
                if i==j: 
                    self.e_cov[i,j] = 1-np.power(self.corM[0,j+1],2)
                else:
                    self.e_cov[i,j] = 1+self.corM[0,i+1]*self.corM[0,j+1]*self.corM[i+1,j+1] - np.power(self.corM[0,i+1],2) - np.power(self.corM[0,j+1],2)
                    self.e_cov[j,i] = self.e_cov[i,j]


    def __sample_select__(self, sample): 
        """
            Extract selected sample for purposes of all, insample and outsample analysis. 
            sample: 'All', 'insample', 'outsample'.  
        """
        if sample == 'All':
            y = copy.copy(self.y)
            yhat = copy.copy(self.yhat)
        elif sample == 'insample':
            y = self.y[0:self.T_training]
            yhat = self.yhat.transpose()[0:self.T_training].transpose()
        elif sample == 'outsample':
            y = self.y[self.T_training: self.T]
            yhat = self.yhat.transpose()[self.T_training:self.T].transpose()
        else:
            print('sample does not exist.\n')
        return y,yhat

    def ferror(self, a, sample='All'):
        """
            Generate forecast errors based on the weight vector a. In the event a is an identity matrix, it will return the variance-covariance matrix of the forecast error from each individaul models.   
        Input:
            a: (K,) numpy array. Weight for the corvariates or an identity matrix. 
            sample: 'All', 'insample', 'outsample'.  
        """
        y,yhat = self.__sample_select__(sample) 
        K,T = yhat.shape
        if len(a.shape) == 1: 
            e = y.reshape((1,T)) - np.dot(a,yhat)
        else:
            i2 = np.ones((a.shape[0],1))
            e = np.kron(i2,y.reshape((1,T))) - np.dot(a, yhat)
            e = np.dot(e,e.transpose())/T
        return e

    def __gen_criteria__(self, a, criteria='MSE', sample='All'):
        """
            Generate a specified forecast criteria based on the input weight. 
        Inputs:
            a: (K,) numpy array. Weight for the corvariates. 
            criteria: 'MSE' or 'MAD'
            sample: 'All', 'insample', 'outsample'.  
        Output:
            crit: criteria. 
        """
        e = self.ferror(a, sample) 
        if criteria == 'MSE':
            return np.mean(np.power(e,2))
        elif criteria == 'MAD':
            return np.mean(abs(e))
        else:
            print('{0} has not been implemented yet'.format(criteria))


    def FOC_MAD(self, a):
        """
        Calculate the first derivative of the MAD objective function for optimal weights. 
        Input:
            a: (1,K+1) numpy arrray. The weight with lambda as the last element. 
        Output:
            [f1,f2]: (K+1,) list. The two partial derivatives of the lagrangian. 

        """
        e = self.ferror(a[0:self.K],sample='insample')
        y,yhat = self.__sample_select__('insample') 
        e_pos = e[e>0]
        e_neg = e[e<0]
        foc = []
        T = self.T_training
        for s in yhat:
            yi = s.reshape((1,T))
            yi_pos = yi[e>0]
            yi_neg = yi[e<0]
            foc.append(-np.sum(yi_pos)/T+np.sum(yi_neg)/T - a[self.K])
        foc.append(1-np.dot(a[0:self.K],np.ones((self.K,1)))[0])
        return foc

    def MC(self, sample):
        """
            Conduct the MC.
        """
        for i in np.arange(0,self.replication):
            self.__gen_data__()
            for j,self.K in enumerate(np.arange(self.incr,self.K_all+self.incr,self.incr)):
                print('This is iteration {0} out of {1} with {2} models out of {3}.\n'.format(i+1,self.replication, self.K, self.K_all))
                self.yhat = self.yhat_all[0:self.K]
                MSE_cov = self.ferror(np.eye(self.K), sample='insample')
                aMSE_true = self.optimal_weight_MSE(self.e_cov[0:self.K, 0:self.K])
                a0 = np.r_[aMSE_true, 0]
                result = spo.fsolve(self.FOC_MAD, a0, full_output=True) 
                aMAD = result[0][0:self.K]
                if self.T_training > self.K+self.incr:
                    aMSE = self.optimal_weight_MSE(MSE_cov) 
                    self.MSE_aMSE[j,i] = self.__gen_criteria__(aMSE, criteria='MSE', sample=sample)
                    self.MAD_aMSE[j,i] = self.__gen_criteria__(aMSE, criteria='MAD', sample=sample)
                self.MSE_aMAD[j,i] = self.__gen_criteria__(aMAD, criteria='MSE', sample=sample)
                self.MAD_aMAD[j,i] = self.__gen_criteria__(aMAD, criteria='MAD', sample=sample)
                self.MSE_aMSE_true[j,i] = self.__gen_criteria__(aMSE_true, criteria='MSE', sample=sample)
                self.MAD_aMSE_true[j,i] = self.__gen_criteria__(aMSE_true, criteria='MAD', sample=sample)

    def MC_analysis(self, showgraph=False, savegraph=False):
        """
        Construct some basic analysis and graphs for the MC results. 
        Note: Showgraph should be False when use with Jupyter notebook
        """

        self.diff_MAD_aMADMSE = np.mean(self.MAD_aMAD, axis=1) - np.mean(self.MAD_aMSE, axis=1) 
        self.diff_MAD_aMADMSE_true = np.mean(self.MAD_aMAD, axis=1) - np.mean(self.MAD_aMSE_true, axis=1)
        self.diff_MSE_aMADMSE = np.mean(self.MSE_aMAD, axis=1) - np.mean(self.MSE_aMSE, axis=1)
        self.diff_MSE_aMADMSE_true = np.mean(self.MSE_aMAD, axis=1) - np.mean(self.MSE_aMSE_true, axis=1) 
        self.diff_MSE_aMSEMSE_true = np.mean(self.MSE_aMSE, axis=1) - np.mean(self.MSE_aMSE_true, axis=1)
        self.diff_MAD_aMSEMSE_true = np.mean(self.MAD_aMSE, axis=1) - np.mean(self.MAD_aMSE_true, axis=1)
        x = np.arange(self.incr, self.K_all+self.incr, self.incr)
        plt.rcParams['figure.figsize'] = [12,12]
        plt.subplot(2,1,1)
        plt.plot(x,self.diff_MAD_aMADMSE_true, x, self.diff_MAD_aMSEMSE_true)
        plt.legend(['Difference between estimated MAD and true MSE weights', 'Difference between estimated MSE and true MSE weights'])
        plt.title('Difference in MAD')
        plt.subplot(2,1,2)
        plt.plot(x,self.diff_MSE_aMADMSE_true, x, self.diff_MSE_aMSEMSE_true)
        plt.legend(['Difference between estimated MAD and true MSE weights', 'Difference between estimated MSE and true MSE weights'])
        plt.title('Difference in MSE')
        if savegraph != False:
            plt.savefig(savegraph)
        plt.rcParams['figure.figsize'] = [12,9]
        if showgraph==True: 
            plt.show()

def save_data(obj, filename='temp.hdf5'): 
    """
    Save results from the MC_MAD_largeK_v2 object. 
    Input: 
        obj: a MC_MAD_largeK_v3 object. 
    Output:
        a HDF5 file
    """
    f = h5.File(filename,'w')
    result = f.create_group('MC_result')
    diff = f.create_group('diff')
    datasetname = ['MSE_aMSE', 'MAD_aMSE', 'MSE_aMAD', 'MAD_aMAD', 'MSE_aMSE_true', 'MAD_aMSE_true']
    diffname = ['diff_MAD_aMADMSE', 'diff_MAD_aMADMSE_true', 'diff_MSE_aMADMSE', 'diff_MSE_aMADMSE_true', 'diff_MSE_aMSEMSE_true', 'diff_MAD_aMSEMSE_true']
    for i,d in enumerate([obj.MSE_aMSE, obj.MAD_aMSE, obj.MSE_aMAD, obj.MAD_aMAD, obj.MSE_aMSE_true, obj.MAD_aMSE_true]):
        #m = result.create_dataset(datasetname(i), d.shape, dtype='float64')
        #m[...]  = d
        result.create_dataset(name=datasetname[i], data=d) 
    for i,d in enumerate([obj.diff_MAD_aMADMSE, obj.diff_MAD_aMADMSE_true, obj.diff_MSE_aMADMSE, obj.diff_MSE_aMADMSE_true, obj.diff_MSE_aMSEMSE_true, obj.diff_MAD_aMSEMSE_true]):
        #m = diff.create_dataset(diffname[i], d.shape, dtype='float64')
        #m[...] = d
        diff.create_dataset(name=diffname[i], data=d) 
    f.close()

def optimal_weight_MSE(y,yhat):
    """
    Compute the optimal wegiths given the variance-covariance matrix of the forecast error. 
    Input:
        y: (1,T) numpy array. The dependent variable. 
        yhat: (K,T) numpy array. The K predicted variables for the T observations. 
    Output:
            weight: (K,) numpy array. The optimal weight vector. 
    """
    K,T = yhat.shape
    e = y - yhat
    cor = np.cov(e)
    i2 = np.ones(K)
    invC = np.linalg.inv(cor)
    A = np.dot(i2.transpose(), np.dot(invC, i2))
    weight = np.dot(invC, i2)/A 
    return weight.reshape(K) 

def optimalWeightMadFoc(a, y, yhat):
    """
    Calculate the first derivative of the MAD objective function for optimal weights. 
    Input:
        a: (1,K+1) numpy arrray. The weight with lambda as the last element. 
        y: (1,T) numpy array. The dependent variable. 
        yhat: (K,T) numpy array. The K predicted variables for the T observations. 
    Output:
        [f1,f2]: (K+1,) list. The two partial derivatives of the lagrangian. 
    
    """
    K,T = yhat.shape
    e = y-np.dot(a[0:K],yhat) 
    e_pos = e[e>0]
    e_neg = e[e<0]
    foc = []
    for s in yhat:
        yi = s.reshape((1,T))
        yi_pos = yi[e>0]
        yi_neg = yi[e<0]
        foc.append(-np.sum(yi_pos)/T+np.sum(yi_neg)/T - a[K])
    foc.append(1-np.dot(a[0:K],np.ones((K,1)))[0])
    return foc

def MAD_linprog(e, positive=True):
    """
    Solving the forecasting combination problem under MAD using linear programing. 
    Inputs:
        e: (N,T) array. A matrix of N forecast errors over T periods. 
        positive: Boolean. If true, it uses the default positivity constraints on the weight. Otherwise, it allows weight to be negative. 
    Outputs:
        mad: Linear programming output - see Scipy.optimize mannual 
    """
    N,T = e.shape #This gives the number of models and the number of observations, respectively. 
    c = np.r_[np.zeros(N),np.ones(2*T)/T] #create the coefficient vector for the objective function. 
    A = np.c_[e.transpose(), -np.eye(T), np.eye(T)] # defining the coefficient matrix. 
    sA = np.c_[np.ones((1,N)), np.zeros(2*T).reshape((1,2*T))]
    A = np.r_[sA,A]
    b = np.r_[1,np.zeros(T)]
    if positive is True:
        bounds = None
    else:
        ebound = [(0,None) for i in range(0,2*T)]
        wbound = [(None,None) for i in range(0,N)]
        bounds = wbound+ebound
    mad = spo.linprog(c,A_eq=A,b_eq=b, bounds=bounds, options={'disp':True})
    return mad

def optimal_weight_MAD(y,yhat, a0=None, linprog=True, positive=True):
    """
    Calculate optimal weight under MAD loss function using Lagrangian function
    Input:
        y: (1,T) numpy array. The dependent variable. 
        yhat: (K,T) numpy array. The K predicted variables for the T observations. 
        a0: (1,K+1) numpy arrray. Initial values of the weight with lambda as the last element. If none, it will use the MSE weight. 
        linprog: Boolean. If True, it uses the linear programming approach, otherwise it solves the FOC. 
        positive: Boolean. Only relevant if linprog == True. If True, linear programming imposes positivity constraints. 
    Output:
        result: (K,) numpy array if linprog is False, otherwise it is a scipy.optimize.linprog outputs. The first K elements are the optimal weight.  
    """

    K,T = yhat.shape
    if linprog is False:
        if a0 == None:
            aInit = optimal_weight_MSE(y,yhat) 
            aInit = np.append(aInit,0)
            aInit = aInit.reshape(K+1)
        else:
            aInit = a0
        foc = lambda a: optimalWeightMadFoc(a, y, yhat)
        result = spo.fsolve(foc, aInit, full_output=True)
    else:
        e = y-yhat
        result = MAD_linprog(e, positive=positive)
    return result

def combineMSEMAE(y,yhat,linprog=True, positive=True, r=0.8, portion=True):
    """
    Function to combine optimal weight from MSE and MAE. The weights are calcuated based on the first r portion of the data, the training set. The 1-r portion will be left out for out-of-sample evaluiation. 
    Inputs:
        y: (1,T) numpy array. The dependent variable. 
        yhat: (K,T) numpy array. The K predicted variables for the T observations. 
        linprog: Boolean. If True, it uses the linear programming approach, otherwise it solves the FOC. 
        positive: Boolean. Only relevant if linprog == True. If True, linear programming imposes positivity constraints. 
        r: float in (0,1). Portion of the data for training. If portion is True, otherwise it is the index of the last observation of the training sample. 
        portion: Boolean. If True, then r is the fraction of sample to be used for training else it is index of the last observation of the training sample. 
    Outputs:
        result: A dictionary with the following keys: 
            MAEWeight: (K,) numpy array. The optimal weight from MAE using first rT observsation. 
            MSEWeight: (K,) numpy array. The optimal weight from MSE using first rT observations. 
            CombineWeight: (K,) numpy array. The average of MAEWeight and MSEWeight. 
            MSEOutSampleMSEWeight: Positive float. Out-of-sample MSE. MSE based on (1-r)T observations using optimal MSE weight
            MAEOutSampleMSEWeight: Positive float. Out-of-sample MAE. MAE based on (1-r)T observations using optimal MSE weight. 
            MSEOutSampleMAEWeight: Positive float. Out-of-sample MSE. MSE based on (1-r)T observations using optimal MAE weight. 
            MAEOutSampleMAEWeight: Positive float. Out-of-sample MAE. MAE based on (1-r)T observations using optimal MAE weight. 
            MSEOutSampleCombineWeight: Positive float. Out-of-sample MSE. MSE based on (1-r)T observatrions using combined weight. 
            MAEOutSampleCombineWeight: Positive float. Out-of-sample MAE. MAE based on (1-r)T observatrions using combined weight. 
            ErrorsMSE: (T,) numpy array. Fit and forecast errors based on the optimal MSE weight. 
            ErrorMAE: (T,) numpy array. Fit and forecast errors based on the optimal MAE weight.  
            ErrorCombine: (T,) numpy array. Fit and forecast errors based on the optimal combine weight. 
            t: int in (0,T), the break point based on numpy.floor(rT). 
    """
    result = {}
    K,T = yhat.shape
    if portion is True:
        t = int(np.floor(r*T))
    else:
        t = r
    yTraining = y[0, 0:t]
    yhatTraining = yhat[:,0:t]
    yTest = y[0, t:T]
    yhatTest = yhat[:,t:T]
    result['t'] = t
    result['MSEWeight'] = optimal_weight_MSE(yTraining,yhatTraining)
    tempResult = optimal_weight_MAD(yTraining,yhatTraining,linprog=linprog, positive=positive).x
    if linprog is True:
        result['MAEWeight'] = tempResult[0:K]
    else:
        result['MAEWeight'] = tempResult
    result['CombineWeight'] = (result['MSEWeight']+result['MAEWeight'])/2
    etest = yTest-yhatTest
    for suffix in ['MSE', 'MAE', 'Combine']:
        weight = suffix+'Weight'
        tempe = np.dot(result[weight], etest)
        result['Errors '+suffix+' Weight'] = y - np.dot(result[weight], yhat)
        for prefix in ['MSE', 'MAE']:
            key = prefix+'OutSample'+suffix+'Weight'
            if prefix is 'MSE':
                result[key] = np.mean(np.power(tempe,2))
            else:
                result[key] = np.mean(abs(tempe))
    return result 

def drawGraphs(y,yhat,result, r=0.8, absolute=False):
    """
    Draw with-in-sample errors and out-of-sample prediction errors. 
    Inputs:
        y: (1,T) numpy array. The dependent variable. 
        yhat: (K,T) numpy array. The K predicted variables for the T observations. 
        result: dict. Result dictionary from the function combineMSEMAE in MC_MAD_largeK_v3.py
        r: float in (0,1). Portion of the data for training. 
        absolute: Boolean. If True, it plots absolute values of errors. 
    """
    K,T = yhat.shape
    e = y-yhat
    t = int(np.floor(r*T))
    eSet = {}
    for prefix in ['MSE', 'MAE', 'Combine']:
        name = prefix+'Weight'
        eSet[prefix] = np.dot(result[name], e)
        if absolute is True:
            eSet[prefix] = abs(eSet[prefix])
    for i,s in enumerate(['Training', 'Test']):
        plt.subplot(2,1,i+1)
        plt.title(s)
        for k in eSet.keys():
            if s == 'Training':
                plt.plot(eSet[k][0:t])
            else:
                plt.plot(eSet[k][t:T])
        plt.legend(['MSE', 'MAE', 'Combine'])
    return None

def getForecastFormat(df, id_name, year, subperiod, fcast, actual):
    """
    Extract relevant columns from the dataframe df and return a (T,K) pandas dataframe with time as index. 
    Inputs:
        df: Pandas dataframe. 
        id_name: str. Label of column contianing forecaster id. 
        year: str. Label of column containing year. 
        subperiod: str. Label of column containing the period within the year (monthly, quarterly..etc). 
        fcast: str. Label of column containing the forecasts. 
        actual: str. Lable of column contianing the actual values. 
    Output:
        result: dataframe. A dataframe with shape (T,K+1A) where T is the number of forecasts and K is the number of forecasters. The additional column contains the actual values. The index are datetime object.
    """
    group_fid = df.groupby(id_name)
    datetimeInfo = df[[year, subperiod]].values
    datetimeInfo = np.c_[datetimeInfo, np.ones(datetimeInfo.shape[0])].astype(int)
    for i,d in enumerate(datetimeInfo):
        if i==0:
            temp = [dt.datetime(*d)]
        else:
            temp.append(dt.datetime(*d))
    df['fcasted_datetime']=temp
    k = 0
    for g,content in group_fid:
        temp = pd.DataFrame(content[fcast].values, index=content['fcasted_datetime'].values, columns=[g])
        temp_actual =pd.DataFrame(content[actual].values, index=content['fcasted_datetime'].values, columns=[str(g)+'_actual']) 
        if k == 0:
            result = temp
            pdactual = temp_actual
        else:   
            result = pd.concat([result,temp], axis=1, join='outer')
            pdactual = pd.concat([pdactual, temp_actual], axis=1, join='outer')
        k=k+1
    for i,c in enumerate(pdactual.columns):
        if i == 0:
            true_actual = pdactual[c]
        else:
            true_actual = true_actual.combine_first(pdactual[c])
    result[actual] = true_actual.values
    return result

def genErrorDataFrame(dataf,fidSet, r=0.8,linprog=True, positive=True):
    """
    Generate the dataframe that contains the forecast errors from the three different optimal weights. 
    Inputs:
        dataf: DataFrame. Format should be identical to those produced by getForecastFormat. 
        fidSet: list. Forecast id to be used in the forecast combination
        linprog: Boolean. If True, it uses the linear programming approach, otherwise it solves the FOC. 
        positive: Boolean. Only relevant if linprog == True. If True, linear programming imposes positivity constraints. 
    Output:
        df: DataFrame. Contians original forecasts, actual values and the forecast erros from the three weights. 
        result. dict. Output from combinMSEMAE. 
    """
    extractlist = fidSet+['actual']
    df = dataf.loc[:,extractlist].dropna()
    y = df['actual'].values.transpose()
    yhat = df.loc[:,fidSet].values.transpose()
    K,T = yhat.shape
    y = y.reshape((1,T))
    result = combineMSEMAE(y,yhat,linprog=linprog, positive=positive,r=r)
    for suffix in ['MSE', 'MAE', 'Combine']:
        name = 'Errors '+suffix+' Weight'
        df[name] = result[name].reshape(T)
    return df, result 

    

