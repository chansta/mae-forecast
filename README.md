# Combining forecasts with mean absolute error scoring rule

This repository contains codes and other supplementary materials for the paper entitled "Combining Forecasts with Mean Absolute Error Scoring rule" by [Felix Chan](mailto:F.Chan@curtin.edu.au) and [Laurent Pauwels](laurent.pauwels@sydney.edu.au). 

## code
    This directory contains some of the codes used to conduct Monte Carlo Experiments in the paper. 

## paper
    This directory contains the supplmentary materials for the paper. 
